﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenericUnitOfWork.cs" company="WolfSoft">
//   All rights reserved.
// </copyright>
// <summary>
//   Defines the GenericUnitOfWork type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GenericUnitOfWork.Library.UnitOfWork
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    using GenericUnitOfWork.Library.Exceptions;
    using GenericUnitOfWork.Library.UnitOfWork.Interfaces;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;

    /// <inheritdoc />
    /// <summary>
    /// Generic unit of work class.
    /// </summary>
    /// <typeparam name="TContext">
    /// Type of the context.
    /// </typeparam>
    public class GenericUnitOfWork<TContext> : IGenericUnitOfWork<TContext> where TContext : DbContext, new()
    {
        /// <summary>
        /// The context.
        /// </summary>
        private TContext entities;

        /// <inheritdoc />
        public virtual TContext Context
        {
            set => this.entities = value;
        }

        /// <inheritdoc />
        public virtual IDbContextTransaction CurrentTransaction { get; set; }

        /// <inheritdoc />
        public virtual IEnumerable<T> GetEntityList<T>() where T : class
        {
            return this.entities.Set<T>().ToList();
        }

        /// <inheritdoc />
        public virtual T Find<T>(object[] keyValues) where T : class
        {
            return this.entities.Set<T>().Find(keyValues);
        }

        /// <inheritdoc />
        public int Add<T>(T model) where T : class
        {
            this.entities.Set<T>().Add(model);
            return this.SaveChanges(typeof(T));
        }

        /// <inheritdoc />
        public int AddRange<T>(IEnumerable<T> modelList) where T : class
        {
            this.entities.Set<T>().AddRange(modelList);
            return this.SaveChanges(typeof(T));
        }

        /// <inheritdoc />
        public int Update<T>(T model) where T : class
        {
            this.entities.Set<T>().Update(model);
            return this.SaveChanges(typeof(T));
        }

        /// <inheritdoc />
        public int UpdateRange<T>(IEnumerable<T> model) where T : class
        {
            this.entities.Set<T>().UpdateRange(model);
            return this.SaveChanges(typeof(T));
        }

        /// <inheritdoc />
        public int Remove<T>(T model) where T : class
        {
            this.entities.Set<T>().Remove(model);
            return this.SaveChanges(typeof(T));
        }

        /// <inheritdoc />
        public int RemoveRange<T>(IEnumerable<T> modelList) where T : class
        {
            this.entities.Set<T>().RemoveRange(modelList);
            return this.SaveChanges(typeof(T));
        }

        /// <inheritdoc />
        public virtual Task<List<T>> GetEntityListAsync<T>() where T : class
        {
            return this.entities.Set<T>().ToListAsync();
        }

        /// <inheritdoc />
        public Task<T> FindAsync<T>(object[] keyValues) where T : class
        {
            return this.entities.Set<T>().FindAsync(keyValues);
        }

        /// <inheritdoc />
        public Task<int> AddAsync<T>(T model) where T : class
        {
            this.entities.Set<T>().AddAsync(model);
            return this.SaveChangesAsync(typeof(T));
        }

        /// <inheritdoc />
        public Task<int> AddRangeAsync<T>(IEnumerable<T> modelList) where T : class
        {
            this.entities.Set<T>().AddRangeAsync(modelList);
            return this.SaveChangesAsync(typeof(T));
        }

        /// <inheritdoc />
        public Task<int> UpdateAsync<T>(T model) where T : class
        {
            this.entities.Update(model);
            return this.SaveChangesAsync(typeof(T));
        }

        /// <inheritdoc />
        public Task<int> UpdateRangeAsync<T>(IEnumerable<T> modelList) where T : class
        {
            this.entities.Set<T>().UpdateRange(modelList);
            return this.SaveChangesAsync(typeof(T));
        }

        /// <inheritdoc />
        public Task<int> RemoveAsync<T>(T model) where T : class
        {
            this.entities.Set<T>().Remove(model);
            return this.SaveChangesAsync(typeof(T));
        }

        /// <inheritdoc />
        public Task<int> RemoveRangeAsync<T>(IEnumerable<T> modelList) where T : class
        {
            this.entities.Set<T>().RemoveRange(modelList);
            return this.SaveChangesAsync(typeof(T));
        }

        /// <inheritdoc />
        public virtual void Dispose()
        {
            this.entities.Dispose();
        }

        /// <inheritdoc />
        public IDbContextTransaction CreateTransaction()
        {
            this.CurrentTransaction = this.entities.Database.BeginTransaction();
            return this.CurrentTransaction;
        }

        /// <inheritdoc />
        public void CommitTransaction(IDbContextTransaction transaction = null)
        {
            var currentTransaction = transaction ?? this.CurrentTransaction;
            currentTransaction.Commit();
        }

        /// <inheritdoc />
        public void RollbackTransaction(IDbContextTransaction transaction = null)
        {
            var currentTransaction = transaction ?? this.CurrentTransaction;
            currentTransaction.Rollback();
        }

        /// <inheritdoc />
        public void DisposeTransaction(IDbContextTransaction transaction = null)
        {
            var currentTransaction = transaction ?? this.CurrentTransaction;
            currentTransaction.Dispose();
        }

        /// <summary>
        /// Saves all changes.
        /// </summary>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int SaveChanges(Type entityType)
        {
            this.ValidateErrors(entityType);
            return this.entities.SaveChanges();
        }

        /// <summary>
        /// Saves all changes asynchronously.
        /// </summary>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private Task<int> SaveChangesAsync(Type entityType)
        {
            this.ValidateErrors(entityType);
            return this.entities.SaveChangesAsync();
        }

        /// <summary>
        /// Validates if there are errors.
        /// </summary>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        /// <exception cref="UoWValidationException">
        /// Throws a unit of work validation exception.
        /// </exception>
        private void ValidateErrors(Type entityType)
        {
            var validationErrors = this.entities.ChangeTracker
                .Entries<IValidatableObject>()
                .SelectMany(e => e.Entity.Validate(null))
                .Where(r => r != ValidationResult.Success)
                .ToList();

            if (validationErrors.Any())
            {
                throw new UoWValidationException(validationErrors, entityType);
            }
        }
    }
}
