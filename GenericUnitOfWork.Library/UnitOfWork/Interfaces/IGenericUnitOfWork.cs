﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGenericUnitOfWork.cs" company="WolfSoft">
//   All rights reserved.
// </copyright>
// <summary>
//   Defines the IGenericUnitOfWork type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GenericUnitOfWork.Library.UnitOfWork.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore.Storage;

    /// <inheritdoc />
    /// <summary>
    /// The GenericUnitOfWork interface.
    /// </summary>
    /// <typeparam name="TContext">
    /// Context type
    /// </typeparam>
    public interface IGenericUnitOfWork<in TContext> : IDisposable
    {
        /// <summary>
        /// Sets the context.
        /// </summary>
        TContext Context { set; }

        /// <summary>
        /// Gets or sets the current transaction.
        /// </summary>
        IDbContextTransaction CurrentTransaction { get; set; }

        #region Synchronous methods
        /// <summary>
        /// Get the entity as a "List"
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <returns>Returns an enumerable of the selected type from the context.</returns>
        IEnumerable<T> GetEntityList<T>() where T : class;

        /// <summary>
        /// Finds the requested entity by matching the keyValues.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="keyValues">The filter.</param>
        /// <returns>Returns the entity matching the key values.</returns>
        T Find<T>(object[] keyValues) where T : class;

        /// <summary>
        /// Adds a model to this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="model">The model which will be added to this context.</param>
        /// <returns>Returns the number of state entries written to the database.</returns>
        int Add<T>(T model) where T : class;

        /// <summary>
        /// Adds a model list to this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="modelList">The model list which will be added to this context.</param>
        /// <returns>Returns the number of state entries written to the database.</returns>
        int AddRange<T>(IEnumerable<T> modelList) where T : class;

        /// <summary>
        /// Update an entity in this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="model">The model which will be updated in this context.</param>
        /// <returns>Returns the number of state entries updated in the database.</returns>
        int Update<T>(T model) where T : class;

        /// <summary>
        /// Updates a model list in this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="modelList">The model list which will be updated in this context.</param>
        /// <returns>Returns the number of state entries updated in the database.</returns>
        int UpdateRange<T>(IEnumerable<T> modelList) where T : class;

        /// <summary>
        /// Deletes the instance of the model from this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in the context.</typeparam>
        /// <param name="model">The model which will be deleted from this context.</param>
        /// <returns>Returns the number of state entries deleted from the database.</returns>
        int Remove<T>(T model) where T : class;

        /// <summary>
        /// Deletes the instance of the model from this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in the context.</typeparam>
        /// <param name="modelList">The model list which will be deleted from this context.</param>
        /// <returns>Returns the number of state entries deleted from the database.</returns>
        int RemoveRange<T>(IEnumerable<T> modelList) where T : class;
        #endregion

        #region Asynchronous methods
        /// <summary>
        /// Gets the entity list asynchronously.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <returns>Returns an enumerable of the selected type from the context.</returns>
        Task<List<T>> GetEntityListAsync<T>() where T : class;

        /// <summary>
        /// Finds the requested entity by matching the keyValues asynchronously.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="keyValues">The filter.</param>
        /// <returns>Returns a task containing the entity matching the key values.</returns>
        Task<T> FindAsync<T>(object[] keyValues) where T : class;

        /// <summary>
        /// Adds a model to this context asynchronously.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="model">The model which will be added to this context.</param>
        /// <returns>Returns the add task.</returns>
        Task<int> AddAsync<T>(T model) where T : class;

        /// <summary>
        /// Adds a model list to this context.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="modelList">The model list which will be added to this context.</param>
        /// <returns>Returns the add range task.</returns>
        Task<int> AddRangeAsync<T>(IEnumerable<T> modelList) where T : class;

        /// <summary>
        /// Update an entity in this context asynchronously.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="model">The model which will be updated in this context.</param>
        /// <returns>Returns the update task.</returns>
        Task<int> UpdateAsync<T>(T model) where T : class;

        /// <summary>
        /// Updates a model list in this context asynchronoulsy.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in this context.</typeparam>
        /// <param name="modelList">The model list which will be updated in this context.</param>
        /// <returns>Returns the update range task.</returns>
        Task<int> UpdateRangeAsync<T>(IEnumerable<T> modelList) where T : class;

        /// <summary>
        /// Deletes the instance of the model from this context asynchronously.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in the context.</typeparam>
        /// <param name="model">The model which will be deleted from this context.</param>
        /// <returns>Returns the remove Task.</returns>
        Task<int> RemoveAsync<T>(T model) where T : class;

        /// <summary>
        /// Deletes the instance of the model from this context asynchronously.
        /// </summary>
        /// <typeparam name="T">Type of the Set used in the context.</typeparam>
        /// <param name="modelList">The model list which will be deleted from this context.</param>
        /// <returns>Returns the remove range task.</returns>
        Task<int> RemoveRangeAsync<T>(IEnumerable<T> modelList) where T : class;
        #endregion

        #region Transaction methods
        /// <summary>
        /// Creates a transaction for this context.
        /// </summary>
        /// <returns>Returns the IDbContextTransaction object.</returns>
        IDbContextTransaction CreateTransaction();

        /// <summary>
        /// Commints the current transaction. If no transaction is receibed by parameter, CurrentTransaction will be used instead.
        /// </summary>
        /// <param name="transaction">The transaction to be commited.</param>
        void CommitTransaction(IDbContextTransaction transaction = null);

        /// <summary>
        /// Rolls back the current transaction. If no transaction is receibed by parameter, CurrentTransaction will be used instead.
        /// </summary>
        /// <param name="transaction">The transaction to rollback.</param>
        void RollbackTransaction(IDbContextTransaction transaction = null);

        /// <summary>
        /// Disposes the current transaction. If no transaction is receibed by parameter, CurrentTransaction will be disposed instead.
        /// </summary>
        /// <param name="transaction">The transaction to be disposed.</param>
        void DisposeTransaction(IDbContextTransaction transaction = null);

        #endregion
    }
}
