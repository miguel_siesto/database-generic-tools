﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UoWValidationException.cs" company="WolfSoft">
//   All rights reserved.
// </copyright>
// <summary>
//   Defines the UoWValidationException type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GenericUnitOfWork.Library.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;

    /// <inheritdoc />
    /// <summary>
    /// Unit of work validation exception class.
    /// </summary>
    public class UoWValidationException : Exception
    {
        /// <summary>
        /// List of the validation errors.
        /// </summary>
        private readonly IEnumerable<ValidationResult> validationErrors;

        /// <summary>
        /// The entity type.
        /// </summary>
        private readonly Type entityType;

        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:GenericUnitOfWork.Library.Exceptions.UoWValidationException" /> class. 
        /// Validates if there are errors.
        /// </summary>
        /// <param name="validationErrors">
        /// The validation Errors.
        /// </param>
        /// <param name="entityType">
        /// The entity type.
        /// </param>
        /// <exception cref="T:GenericUnitOfWork.Library.Exceptions.UoWValidationException">
        /// Throws a unit of work validation exception.
        /// </exception>
        public UoWValidationException(IEnumerable<ValidationResult> validationErrors, Type entityType)
        {
            var validationResults = validationErrors as IList<ValidationResult> ?? validationErrors.ToList();
            this.validationErrors = validationResults;
            this.entityType = entityType;
        }

        /// <inheritdoc />
        public override string Message => this.CreateExceptionBody();

        /// <summary>
        /// Creates exception body.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string CreateExceptionBody()
        {
            var sb = new StringBuilder();
            sb.Append($"Error while trying to save entity type: {this.entityType}");
            foreach (var error in this.validationErrors)
            {
                sb.Append($"\n\t-{error.ErrorMessage}");
            }

            return sb.ToString();
        }
    }
}
