﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenericRepository.cs" company="WolfSoft">
//   All rights reserved.
// </copyright>
// <summary>
//   The generic repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GenericRepository.Library.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using Interfaces;

    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// The _dbSet model set.
        /// </summary>
        private readonly DbSet<TEntity> dbset;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        internal GenericRepository(DbSet<TEntity> value)
        {
            this.dbset = value;
        }

        /// <inheritdoc />
        public virtual IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> query = this.dbset;
            return query;
        }

        /// <inheritdoc />
        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = this.dbset;

            query = filter != null ? query.Where(filter) : query;

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return orderBy?.Invoke(query).ToList() ?? query.ToList();
        }

        /// <inheritdoc />
        public IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> predicate)
        {
            var query = this.dbset.Where(predicate);
            return query;
        }

        /// <inheritdoc />
        public virtual bool Find(TEntity entity)
        {
            return this.dbset.Find(entity) != null;
        }

        /// <inheritdoc />
        public virtual void Add(TEntity entity)
        {
            this.dbset.Add(entity);
        }

        /// <inheritdoc />
        public virtual void Delete(TEntity entity)
        {
            this.dbset.Remove(entity);
        }

        /// <inheritdoc />
        public virtual void Edit(TEntity entity)
        {
            this.dbset.Update(entity);
        }
    }
}
