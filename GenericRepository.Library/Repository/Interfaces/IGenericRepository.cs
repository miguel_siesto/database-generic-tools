﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGenericRepository.cs" company="WolfSoft">
//   All rights reserved.
// </copyright>
// <summary>
//   Defines the IGenericRepository type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GenericRepository.Library.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// The GenericRepository interface.
    /// </summary>
    /// <typeparam name="TEntity"> Entity type
    /// </typeparam>
    internal interface IGenericRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Get all results.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Get results by criteria.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="orderBy">
        /// The order by.
        /// </param>
        /// <param name="includeProperties">
        /// The include properties.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/>.
        /// </returns>
        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// The select.
        /// </summary>
        /// <param name="predicate">
        /// The predicate.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Adds an entity to the set.
        /// </summary>
        /// <param name="entity">
        /// The entity to be added.
        /// </param>
        void Add(TEntity entity);

        /// <summary>
        /// Finds an entity in the set.
        /// </summary>
        /// <param name="entity">
        /// The entity to be found.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Find(TEntity entity);

        /// <summary>
        /// Deletes the entity from the set.
        /// </summary>
        /// <param name="entity">
        /// The entity to be deleted.
        /// </param>
        void Delete(TEntity entity);

        /// <summary>
        /// Edits the entity in the set.
        /// </summary>
        /// <param name="entity">
        /// The entity to be modified.
        /// </param>
        void Edit(TEntity entity);
    }
}
